<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

# Ejecutar en desarrollo

1. Clonar el repositorio
2. Ejecutar

```
yarn install
```

3. Tener Nest CLI instalado

```
npm i -g @nestjs/cli
```

4. Crear archivo **.env** con el contenido de **.env.example**

5. Levantar base de datos

```
docker-compose up -d
```

6. Levantar aplicación

```
yarn start:dev
```

7. Popular la base de datos con seed de 151 pokémons

```
http://localhost:3000/api/seed
```

# Build de producción dockerizado

1. Crear la nueva imágen:

```
docker-compose -f docker-compose.prod.yaml --env-file .env.prod up --build
```

# Stack utilizado

- MongoDB 5
- NestJS 10.0.0
- Docker Desktop (en caso de querer dockerizar la app)

# Endpoints y utilización

1. Get de pokemon por id de mongo, nombre o número

```
GET http://localhost:3000/api/pokemon/25
GET http://localhost:3000/api/pokemon/ID_VALIDO_DE_MONGO_DB
GET http://localhost:3000/api/pokemon/pikachu
```

2. Post de pokemon

```
POST http://localhost:3000/api/pokemon
{
  "name": string,
  "no": number
}

```

3. UPDATE pokemon por nombre o número

```
PATCH http://localhost:3000/api/pokemon/25
PATCH http://localhost:3000/api/pokemon/ID_VALIDO_DE_MONGO_DB
PATCH http://localhost:3000/api/pokemon/pikachu

{
  "name": string,
  "no": number
}

```

4. DELETE pokemon por id de mongo

```
DELETE http://localhost:3000/api/pokemon/ID_VALIDO_DE_MONGO_DB
```

5. Paginación

```
localhost:3000/api/pokemon?limit=25&offset=10

limit especifica la cantidad de registros, y el offset desde qué registro se espera que llegue
```
