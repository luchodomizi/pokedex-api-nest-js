import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { firstValueFrom } from 'rxjs';

import { PokeResponse } from './interfaces/poke-response.interface';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Pokemon } from 'src/pokemon/entities/pokemon.entity';

@Injectable()
export class SeedService {
  constructor(
    private readonly httpService: HttpService,

    @InjectModel(Pokemon.name)
    private readonly pokemonModel: Model<Pokemon>,
  ) {}
  async executeSeed() {
    await this.pokemonModel.deleteMany({});

    const pokemonToInsert: { name: string; no: number }[] = [];

    try {
      const { data } = await firstValueFrom(
        this.httpService.get<PokeResponse>(
          'https://pokeapi.co/api/v2/pokemon?limit=151',
        ),
      );

      data.results.forEach(async ({ name, url }) => {
        const segments = url.split('/');
        const no = +segments[segments.length - 2];

        pokemonToInsert.push({ name, no });
      });

      this.pokemonModel.insertMany(pokemonToInsert);

      return 'Seed Executed';
    } catch (error) {
      console.log(error);
    }
  }
}
